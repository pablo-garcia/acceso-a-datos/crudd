
<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <h1><b>Consutas de seleccion<b/></h1>
        <h2><i>Módulo 2 - unidad 3</i></h2> <br><br><br>

  <div class="d-flex justify-content-center">
   <div class="row">
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 1</h2>
        <p>listar las edades de los ciclistas (sin repetidos)</p>
      <?= Html::a('Active Record', ['/site/consulta1'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?> <br><br>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 2</h2>
        <p>listar las edades de los ciclistas de Artiach</p>
      <?= Html::a('Active Record', ['/site/consulta2'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>        <br><br>
      </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 3</h2>
        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
            <?= Html::a('Active Record', ['/site/consulta3'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>        <br><br>
        </div>
    </div>
      <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 4</h2>
        <p>listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
            <?= Html::a('Active Record', ['/site/consulta4'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>        <br><br>
        </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 5</h2>
        <p>listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto
</p>
      <?= Html::a('Active Record', ['/site/consulta5'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>        <br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 6</h2>
        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
      <?= Html::a('Active Record', ['/site/consulta6'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>        <br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta 7</h2>
        <p>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas
</p>
      <?= Html::a('Active Record', ['/site/consulta7'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>         <br><br>
      </div>
    </div>
      <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta 8</h2>
        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
      <?= Html::a('Active Record', ['/site/consulta8'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>         <br><br>
      </div>
      </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta 9</h2>
        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500</p>
      <?= Html::a('Active Record', ['/site/consulta9'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', [''], ['class' => 'btn btn-default']) ?>          <br><br>
      </div>
    </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta 10</h2>
        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
      <?= Html::a('Active Record', ['/site/consulta10'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/ciclista/index'], ['class' => 'btn btn-default']) ?>          <br><br>
      </div>
    </div>
    <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta 11</h2>
        <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
      <?= Html::a('Active Record', ['/site/consulta11'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/ciclista/index'], ['class' => 'btn btn-default']) ?>          <br><br>
      </div>
    </div>
    </div>
</div>

