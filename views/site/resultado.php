<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
?>


<h1> <?= $titulo ?> </h1>     
<h3><i> <?= $enunciado ?> </i></h2>
    
   <h4> <?= $sql ?> </h3>


<?php 
    use \yii\widgets\Pjax;
    Pjax::begin(); // <-- Instancia del Pjax ?>
	<?= GridView::widget([
		'dataProvider' => $resultados,
		'columns' => $campos
	]); ?>
<?php Pjax::end(); ?>