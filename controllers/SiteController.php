<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\Ciclista;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionConsulta1(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
}
    public function actionConsulta2(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'" ),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 2 con active record",
            "enunciado" => "listar las edades de los ciclistas de artiach",
            "sql" => "Select distinct edad from ciclista where nombre = 'artiach'",
            
        ]);
}   
     public function actionConsulta3(){
        
        $dataprovider = new ActiveDataProvider([
            
           'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach' or nomequipo = 'Amore Vita'" ),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 3 con active record",
            "enunciado" => "listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql" => "Select distinct edad from ciclista where nomequipos = 'Amore Vita or nomequipo = 'Artiach'",
            
        ]);
} 
    public function actionConsulta4(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad < 25 or edad > 30"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 4 con active record",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql" => "Select distinct dorsal from ciclista where edad < 25 or edad > 30",
            
        ]);
} 
    public function actionConsulta5(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad between 28 and 32 and nombre = 'Banesto'"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 5 con active record",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto ",
            "sql" => "Select distinct dorsal from ciclista where edad between 28 and 32 and nomequipo = 'Banesto'",
            
        ]);
} 
    public function actionConsulta6(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("nombre")->distinct()->where("CHAR_LENGTH(nombre) > 8"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['nombre'],
            "titulo" => "Consulta 6 con active record",
            "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql" => "Select distinct nombre from ciclista where CHAR_LENGTH(nombre) > 8",
            
        ]);
} 
    public function actionConsulta7(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("nombre as nombre_mayusculas,dorsal")->distinct().upper(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['nombre','dorsal'],
            "titulo" => "Consulta 7 con active record",
            "enunciado" => "lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql" => "select upper(nombre) as nombre_mayusculas, dorsal  from ciclista;",
            
        ]);
} 
    public function actionConsulta8(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
} 
    public function actionConsulta9(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
} 
    public function actionConsulta10(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
} 
    public function actionConsulta11(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
} 

    /** 
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
            
        
        public function actionEntry() {
            
            $model = new EntryForm();
            
            if($model->load(Yii::$app->request->post()) && $model->validate()){
            //validar los datos recibidos del modelo
                
                return $this->render('entry-confirm',['model'=> $model]);
            } else {
                
                return $this->render('entry',['model' => $model]);
                
                
            }
            
        }
        
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
